import pytest
import requests
import allure
import uuid
import os
import time

def test_win():
    test_ip = "http://" + os.environ['TEST_HOST']
    resp = requests.post(test_ip,
             headers={"Source-Type":"game"},
             json={"state": "win", "amount": "10.15", "transactionId": 
             str(uuid.uuid1(clock_seq=int(time.time()* 1000)))  } )
    print(resp.text)
    print(str(uuid.uuid1(clock_seq=int(time.time()* 1000))))
    assert resp.status_code == 200

def test_lost():
    test_ip = "http://" + os.environ['TEST_HOST']
    resp = requests.post(test_ip,
             headers={"Source-Type":"game"},
             json={"state": "lost", "amount": "10.15", "transactionId": 
             str(uuid.uuid1(clock_seq=int(time.time()* 1000) )) } )
    print(resp.text)
    print(str(uuid.uuid1(clock_seq=int(time.time()* 1000))))
    assert resp.status_code == 200