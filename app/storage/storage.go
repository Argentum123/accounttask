package storage

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	_ "github.com/lib/pq"
)

type Storage struct {
	dbConnPool    *pgxpool.Pool
	endpoint      string
	migrationPath string
	period        uint16
}

func NewStorage(endpoint, migrationPath string, period uint16) (st Storage, err error) {
	st.endpoint = endpoint
	st.migrationPath = migrationPath
	conn, err := pgxpool.Connect(context.Background(), endpoint)
	if err != nil {
		return
	}
	st.dbConnPool = conn

	db, err := sql.Open("postgres", endpoint)
	defer db.Close()
	if err != nil {
		return
	}
	driver, err := postgres.WithInstance(db, &postgres.Config{})
	if err != nil {
		return
	}
	m, err := migrate.NewWithDatabaseInstance(
		migrationPath,
		"postgres", driver)
	if err != nil {
		return
	}

	err = m.Down()
	if err != nil && err != migrate.ErrNoChange {
		return
	}
	err = m.Up()
	if err != nil && err != migrate.ErrNoChange {
		return
	}
	return st, nil
}

// Down down migrations. Helps to test package
func (st *Storage) Down() (err error) {
	db, err := sql.Open("postgres", st.endpoint)
	defer db.Close()
	if err != nil {
		return
	}
	driver, err := postgres.WithInstance(db, &postgres.Config{})
	if err != nil {
		return
	}
	m, err := migrate.NewWithDatabaseInstance(
		st.migrationPath,
		"postgres", driver)
	if err != nil {
		return
	}
	err = m.Down()
	if err != nil && err != migrate.ErrNoChange {
		return
	}
	st.dbConnPool.Close()
	return
}

func (st *Storage) Win(ctx context.Context, id uuid.UUID, money float64) error {
	if money <= 0 {
		return fmt.Errorf("the amount of money should be positive")
	}
	tx, err := st.dbConnPool.Begin(ctx)
	if err != nil {
		return err
	}
	defer tx.Rollback(ctx)

	_, err = tx.Exec(ctx, `LOCK TABLE transactions in ACCESS EXCLUSIVE  MODE;`)
	if err != nil {
		return err
	}
	_, err = tx.Exec(ctx,
		"INSERT INTO transactions (tr_id, ts, tr_money) VALUES($1, NOW(), $2)", id, money)
	if err != nil {
		return err
	}
	err = tx.Commit(ctx)
	return err
}

func (st *Storage) Lost(ctx context.Context, id uuid.UUID, money float64) error {
	if money <= 0 {
		return fmt.Errorf("the amount of money should be positive")
	}
	tx, err := st.dbConnPool.Begin(ctx)
	if err != nil {
		return err
	}
	defer tx.Rollback(ctx)

	_, err = tx.Exec(ctx, `LOCK TABLE transactions in ACCESS EXCLUSIVE  MODE;`)
	if err != nil {
		return err
	}
	row, err := tx.Exec(ctx, `INSERT INTO transactions (tr_id, ts, tr_money) 
											SELECT $1, NOW(), $2 FROM transactions
											HAVING sum(tr_money) + $2 >= 0 `, id, -money)
	if err != nil {
		return err
	}
	if row.RowsAffected() == 0 {
		return fmt.Errorf("not enough funds in the account")
	}
	err = tx.Commit(ctx)
	return err
}

func (st *Storage) postProcessStart(period float64) {
	go func(postProcessePeriod float64) {
		st.PostProcess(postProcessePeriod)
		time.Sleep(time.Duration(postProcessePeriod) * time.Minute)
	}(period)

}

func (st *Storage) GetBalcance(ctx context.Context) (float64, error) {
	var balance float64
	err := st.dbConnPool.QueryRow(ctx, `SELECT sum(tr_money) FROM transactions`).Scan(&balance)
	if err != nil {
		return 0, err
	}
	return balance, nil
}

func (st *Storage) StartPostProcess() {
	go func() {
		for {
			time.Sleep(time.Minute * time.Duration(st.period/10.0))
			st.PostProcess(float64(st.period))
		}
	}()
}

func (st *Storage) PostProcess(postProcessePeriod float64) error {
	ctx := context.Background()
	tx, err := st.dbConnPool.Begin(ctx)
	if err != nil {
		return err
	}
	defer tx.Rollback(ctx)

	_, err = tx.Exec(ctx, `LOCK TABLE postprocces in ACCESS EXCLUSIVE  MODE;`)
	if err != nil {
		return err
	}
	_, err = tx.Exec(ctx, `LOCK TABLE transactions in ACCESS EXCLUSIVE  MODE;`)
	if err != nil {
		return err
	}

	// Check if it's time to do post process job
	row, err := tx.Exec(ctx,
		`INSERT INTO postprocces (ts) 
		 SELECT  NOW() FROM postprocces
		 HAVING (
			 select  EXTRACT(EPOCH FROM (NOW() -ts))/60 as diff
					FROM postprocces order by ts DESC LIMIT 1) > $1;`, postProcessePeriod)
	if err != nil {
		return err
	}
	if row.RowsAffected() == 0 {
		return fmt.Errorf("it's too early")
	}

	row, err = tx.Exec(ctx,
		`with last_cancel_time as (
			SELECT ts from postprocces order By ts DESC limit 1 offset 1
		),
		unprocessed_records as (
		select *  from 
			transactions as tr
					where (select  ts from last_cancel_time) < tr.ts
		),
		records_to_delete as (select id from  unprocessed_records     
			where id % 2 = 1 order by ts DESC  LIMIT 10),
		sum_of_delete_records as (select sum(tr_money) from transactions
			where id in (select * from records_to_delete)
		)
		delete from  transactions where id in (select * from records_to_delete)
		 and (select sum(tr_money) from transactions) > (SELECT * from sum_of_delete_records);`)
	if row.RowsAffected() == 0 {
		return fmt.Errorf("no records has been canceled")
	}

	err = tx.Commit(ctx)
	return err
}
