package storage

import (
	"context"
	"math/rand"
	"sync"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func setup() (Storage, error) {
	return NewStorage("postgres://localhost:5432/transactions?user=admin&password=admin&sslmode=disable",
		"file:..//db/migrations")
}

func shutdown(st Storage) error {
	return st.Down()
}

func TestFirstWin(t *testing.T) {
	st, err := setup()
	assert.Nil(t, err)

	uID, _ := uuid.NewRandom()
	err = st.Win(context.Background(), uID, 10)
	assert.Nil(t, err)

	shutdown(st)
}

func TestFirstLost(t *testing.T) {
	st, err := setup()
	assert.Nil(t, err)

	uID, _ := uuid.NewRandom()
	err = st.Lost(context.Background(), uID, 10)
	assert.NotNil(t, err)

	shutdown(st)
}

func TestWinMoreThenLost(t *testing.T) {
	st, err := setup()
	assert.Nil(t, err)

	uID, _ := uuid.NewRandom()
	err = st.Win(context.Background(), uID, 11)
	assert.Nil(t, err)
	uID, _ = uuid.NewRandom()
	err = st.Lost(context.Background(), uID, 10)
	assert.Nil(t, err)

	shutdown(st)
}

func TestWinLessThenLost(t *testing.T) {
	st, err := setup()
	assert.Nil(t, err)

	uID, _ := uuid.NewRandom()
	err = st.Win(context.Background(), uID, 10)
	assert.Nil(t, err)
	uID, _ = uuid.NewRandom()
	err = st.Lost(context.Background(), uID, 11)
	assert.NotNil(t, err)
	shutdown(st)
}

func TestNoRetryesAllowed(t *testing.T) {
	st, err := setup()
	assert.Nil(t, err)

	uID, _ := uuid.NewRandom()
	err = st.Win(context.Background(), uID, 10)
	assert.Nil(t, err)
	err = st.Win(context.Background(), uID, 10)
	assert.NotNil(t, err)

	shutdown(st)
}

func TestParallel(t *testing.T) {

	for times := 0; times < 100; times++ {
		st, err := setup()
		assert.Nil(t, err)

		wg := sync.WaitGroup{}
		wg.Add(20)
		for thread := 0; thread < 20; thread++ {
			go func(st *Storage, wg *sync.WaitGroup) {
				err := randomRecord(st)
				assert.Nil(t, err)
				wg.Done()
			}(&st, &wg)
		}
		wg.Wait()
		balance, err := st.GetBalcance(context.Background())
		assert.Nil(t, err)
		assert.GreaterOrEqual(t, balance, 0.0)

		err = shutdown(st)
		assert.Nil(t, err)
	}

}

func TestPostProcessDeletes(t *testing.T) {

	st, err := setup()
	assert.Nil(t, err)
	for i := 0; i < 100; i++ {
		uID, _ := uuid.NewRandom()
		err := st.Win(context.Background(), uID, 10)
		assert.Nil(t, err)
	}
	time.Sleep(time.Second)
	err = st.PostProcess(1.0 / 60)
	assert.Nil(t, err)
	balance, err := st.GetBalcance(context.Background())
	assert.Nil(t, err)
	assert.Equal(t, 900.0, balance)

	err = shutdown(st)
	assert.Nil(t, err)
}

func TestPostProcessToEarly(t *testing.T) {

	st, err := setup()
	assert.Nil(t, err)

	for i := 0; i < 100; i++ {
		uID, _ := uuid.NewRandom()
		err := st.Win(context.Background(), uID, 10)
		assert.Nil(t, err)
	}
	err = st.PostProcess(2.0 / 60)
	assert.NotNil(t, err)

	err = shutdown(st)
	assert.Nil(t, err)
}

func TestPostProcessDeletesSome(t *testing.T) {

	st, err := setup()
	assert.Nil(t, err)

	for i := 0; i < 100; i++ {
		uID, _ := uuid.NewRandom()
		err := st.Win(context.Background(), uID, 10)
		assert.Nil(t, err)
	}

	for i := 0; i < 100; i++ {
		uID, _ := uuid.NewRandom()
		if i%2 == 0 {
			err = st.Win(context.Background(), uID, 10)
		} else {
			err = st.Lost(context.Background(), uID, 10)
		}
		assert.Nil(t, err)
	}

	time.Sleep(time.Second)
	err = st.PostProcess(1.0 / 60)
	assert.Nil(t, err)

	balance, err := st.GetBalcance(context.Background())
	assert.Nil(t, err)
	assert.Equal(t, 900.0, balance)

	err = shutdown(st)
	assert.Nil(t, err)
}

func TestPostProcessDelete3from5(t *testing.T) {

	st, err := setup()
	assert.Nil(t, err)

	for i := 0; i < 5; i++ {
		uID, _ := uuid.NewRandom()
		err := st.Win(context.Background(), uID, 100)
		assert.Nil(t, err)
	}

	time.Sleep(time.Second)
	err = st.PostProcess(1.0 / 60)
	assert.Nil(t, err)

	balance, err := st.GetBalcance(context.Background())
	assert.Nil(t, err)
	assert.Equal(t, 200.0, balance)

	err = shutdown(st)
	assert.Nil(t, err)
}

// ~1.5 ms per transaction
func BenchmarkWrite(b *testing.B) {
	st, err := setup()
	assert.Nil(b, err)
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		err := randomRecord(&st)
		assert.Nil(b, err)
	}
	b.StopTimer()
	shutdown(st)
}

func randomRecord(st *Storage) error {
	uID, _ := uuid.NewRandom()
	amount := rand.Float64()
	if amount > 0 {
		err := st.Win(context.Background(), uID, amount)
		if err != nil {
			return err
		}
	} else if amount < 0 {
		st.Lost(context.Background(), uID, amount)
	}
	return nil
}
