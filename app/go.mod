module app

go 1.13

require (
	github.com/golang-migrate/migrate v3.5.4+incompatible
	github.com/golang-migrate/migrate/v4 v4.7.1
	github.com/google/uuid v1.1.1
	github.com/jackc/pgx/v4 v4.1.2
	github.com/lib/pq v1.2.0
	github.com/mattes/migrate v3.0.1+incompatible
	github.com/stretchr/testify v1.4.0
	gopkg.in/yaml.v2 v2.2.2
)
