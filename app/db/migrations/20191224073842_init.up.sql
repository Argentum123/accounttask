
CREATE TABLE IF NOT EXISTS transactions (
   id serial PRIMARY KEY,
   ts TIMESTAMP,
   tr_id UUID UNIQUE,
   tr_money double precision
);

CREATE UNIQUE INDEX  IF NOT EXISTS tr_id_index ON transactions (tr_id);
CREATE INDEX   IF NOT EXISTS tr_ts_index ON transactions (ts);

CREATE TABLE IF NOT EXISTS postprocces (
   id serial PRIMARY KEY,
   ts TIMESTAMP
);

CREATE  UNIQUE INDEX  IF NOT EXISTS pp_ts_index ON postprocces (ts);

INSERT  into postprocces(ts) VALUES(NOW());