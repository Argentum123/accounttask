package main

import (
	"app/server"
	"app/storage"
	"fmt"
	"os"

	"gopkg.in/yaml.v2"
)

type Config struct {
	Server struct {
		Port    uint16   `yaml:"port"`
		Timeout uint16   `yaml:"timeout"`
		Headers []string `yaml:"headers"`
		} `yaml:"server"`
	Storage struct {
		Period  uint16   `yaml:"period"`
		Host     string `yaml:"host"`
		Username string `yaml:"user"`
		Password string `yaml:"password"`
		DbName   string `yaml:"dbname"`
		Port     uint16 `yaml:"port"`
	} `yaml:"storage"`
}

func main() {
	cfg := ParseConfig()
	dbConnection := fmt.Sprintf("postgres://%s:%d/%s?user=%s&password=%s&sslmode=disable",
		cfg.Storage.Host, cfg.Storage.Port, cfg.Storage.DbName, cfg.Storage.Username, cfg.Storage.Password)

	st, err := storage.NewStorage(dbConnection, "file://db/migrations", cfg.Storage.Period)
	if err != nil {
		panic(err)
	}
	serv := server.NewServer(&st, cfg.Server.Port, cfg.Server.Timeout, cfg.Server.Headers)
	serv.ServerStart()
}

func ParseConfig() Config {
	f, err := os.Open("config.yml")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	var cfg Config
	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(&cfg)
	if err != nil {
		panic(err)
	}
	return cfg
}
