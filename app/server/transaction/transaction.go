package transaction

import (
	"encoding/json"
	"fmt"
	"strconv"

	"github.com/google/uuid"
)

var (
	states = map[string]bool{"win": true, "lost": true}
)

type Transaction struct {
	State         string `json="state"`
	Amount        string `json="amount"`
	TransactionId string `json="transactionId"`
	Uid           uuid.UUID
	AmountFloat   float64
}

func (tr *Transaction) Win() bool {
	return tr.State == "win"
}

func Parse(bArray []byte) (tr Transaction, err error) {
	err = json.Unmarshal(bArray, &tr)
	if err != nil {
		err = fmt.Errorf("failed to parse transaction; error: %s", err.Error())
		return
	}
	if _, ok := states[tr.State]; !ok {
		err = fmt.Errorf("state %s is unknown", tr.State)
		return
	}
	if _, err = uuid.Parse(tr.TransactionId); err != nil {
		err = fmt.Errorf("transaction id is not a valid uuid. err: %s", err.Error())
		return
	}
	tr.AmountFloat, err = strconv.ParseFloat(tr.Amount, 64)
	return
}
