package server

import (
	"app/server/transaction"
	"app/storage"
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

type Server struct {
	storage     *storage.Storage
	port        uint16
	headerTypes map[string]bool
	timeout     uint16
}

func NewServer(st *storage.Storage, port, timeout uint16, headerTypes []string) (s Server) {
	s.storage = st
	s.storage.StartPostProcess()
	s.port = port
	s.timeout = timeout
	s.headerTypes = make(map[string]bool)
	for _, headerType := range headerTypes {
		s.headerTypes[headerType] = true
	}
	return
}

func (s *Server) ServerStart() {
	http.HandleFunc("/", s.handler)
	log.Printf("Server starts on port %d\n", s.port)
	http.ListenAndServe(fmt.Sprintf(":%d", s.port), nil)
}

func (s *Server) handler(w http.ResponseWriter, r *http.Request) {
	sourceType := r.Header.Get("Source-Type")
	if _, exists := s.headerTypes[sourceType]; !exists {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(fmt.Sprintf("Unexpected source type %s ", sourceType)))
		return
	}
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	tr, err := transaction.Parse(body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	ctx, cancel := context.WithTimeout(r.Context(), time.Second*time.Duration(s.timeout))
	defer cancel()
	if tr.Win() {
		err = s.storage.Win(ctx, tr.Uid, tr.AmountFloat)
	} else {
		err = s.storage.Lost(ctx, tr.Uid, tr.AmountFloat)
	}
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("Request processed"))
	w.WriteHeader(http.StatusOK)
}

func headerValidation() error {
	return nil
}
